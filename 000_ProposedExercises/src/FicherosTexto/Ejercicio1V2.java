 package FicherosTexto;

import java.io.*;
import java.util.Scanner;


public class Ejercicio1V2 {
	

	static int[] numeros;
	
	
	static clases_comunes comun = new clases_comunes();
	
	
		
	
	public static void escribirFichero(String nombre) {
		
		try {
			File fichero = new File(nombre);
			FileWriter fw = new FileWriter(fichero);

			
			for(int cnt=0; cnt<numeros.length; cnt++) {
				fw.write(numeros[cnt]);
			}
			
			fw.close();
		} catch (IOException e) { e.printStackTrace(); System.out.println(e.getMessage()); }
		
	}
	
	
	
	public static void leerFichero(String nombre) {
		try {
			File fichero = new File(nombre);
			FileReader fr = new FileReader(fichero);
			
			int letra;
			int cnt=0;
			
			letra = fr.read();
			while (letra != -1 && cnt<10 ) {
				if(letra >= '0' && letra <= '9') {
					numeros[cnt] =  letra - '0';
					letra = fr.read();
					cnt++;
				}else { 
					System.out.println("No es un numero");
					letra = fr.read();
					cnt++;
				}
			}
			
			fr.close();
			
			System.out.println("He leido: ");
			for(int co=0; co<numeros.length; co++) {
				System.out.println(numeros[co]);
			}
			
		} catch (FileNotFoundException e) { e.printStackTrace(); System.out.println(e.getMessage()); 
		} catch (IOException e) 		  { e.printStackTrace(); System.out.println(e.getMessage());}


	}
	
	
	

	public static void main(String[] args) {
		
		String nameFile = "document.txt";
		
		Scanner scan = new Scanner(System.in);
		
		int opcion = 0;
		numeros = new int[10];
		
		do {
			
			comun.menu();
			opcion = scan.nextInt();
			switch(opcion) {
				case 1:
					System.out.println("Case1");
					
	
					for(int cnt=0; cnt<10; cnt++) {
						Scanner scan2 = new Scanner(System.in);
						int data = scan2.nextInt();
						numeros[cnt] = data;
	
					}
					escribirFichero(nameFile);
					
				break;
				
				case 2:
					leerFichero(nameFile);
					break;
					
				case 3:
					comun.mostrarTabla();
					break;
				
				case 4:
					Scanner scan3 = new Scanner(System.in);
					Scanner scan4 = new Scanner(System.in);
					System.out.println("Indique que elemento de la lista desea modificar: ");
					int posic = scan3.nextInt();
					System.out.println("Indique el nuevo numero");
					int nuevo = scan4.nextInt();
					
					numeros[posic] = nuevo;
					
				break;
				
				case 5:
					escribirFichero(nameFile);
					break;
			}
			
		}while(opcion != 6);
		
		
		

	}

}
