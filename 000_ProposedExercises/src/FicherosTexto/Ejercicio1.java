package FicherosTexto;

import java.io.*;
import java.util.Scanner;
public class Ejercicio1 {
	
	static String cadena = "";
	static int[] numeros;
	
	
	public static void menu() {
		System.out.println("\t MENU \t");
		System.out.println("--------------------");
		System.out.println("1. - EscribirFichero");
		System.out.println("2. - LeerFichero");
		System.out.println("3. - Escribir");
		System.out.println("4. - ModificarTabla");
		System.out.println("5. - ActualizarTablaFichero");
		System.out.println("6. - Salir");
	}
	
	
	
		
	
	public static void escribirFichero(String nombre) {
		
		try {
			File fichero = new File(nombre);
			FileWriter fw = new FileWriter(fichero);

			
			for(int cnt=0; cnt<numeros.length; cnt++) {
				cadena += numeros[cnt];
			}
			fw.write(cadena + "\n");
			fw.close();
		} catch (IOException e) { e.printStackTrace(); System.out.println(e.getMessage()); }
		
	}
	
	
	
	public static void leerFichero(String nombre) {
		try {
			File fichero = new File(nombre);
			FileReader fr = new FileReader(fichero);
			
			int letra;
			
			
			letra = fr.read();
			while (letra != -1) {
				cadena += ((char) letra);
				letra = fr.read();
			}
			
			fr.close();
			System.out.println("He leido: " + cadena);
			
		} catch (FileNotFoundException e) { e.printStackTrace(); System.out.println(e.getMessage()); 
		} catch (IOException e) 		  { e.printStackTrace(); System.out.println(e.getMessage());}


	}
	
	
	
	public static void mostrarTabla() {
		for(int cnt=0; cnt<10; cnt++) {
			System.out.println(numeros[cnt]);
		}
	}
	
	
	
	
	
	

	public static void main(String[] args) {
		
		String nameFile = "document.txt";
		
		Scanner scan = new Scanner(System.in);
		
		int opcion = 0;
		
		do {
			
			menu();
			opcion = scan.nextInt();
			switch(opcion) {
				case 1:
					System.out.println("Case1");
					numeros = new int[10];
	
					for(int cnt=0; cnt<10; cnt++) {
						Scanner scan2 = new Scanner(System.in);
						int data = scan2.nextInt();
						numeros[cnt] = data;
	
					}
					escribirFichero(nameFile);
					
				break;
				
				case 2:
					leerFichero(nameFile);
					break;
					
				case 3:
					mostrarTabla();
					break;
				
				case 4:
					Scanner scan3 = new Scanner(System.in);
					Scanner scan4 = new Scanner(System.in);
					System.out.println("Indique que elemento de la lista desea modificar: ");
					int posic = scan3.nextInt();
					System.out.println("Indique el nuevo numero");
					int nuevo = scan4.nextInt();
					
					numeros[posic] = nuevo;
					
				break;
				
				case 5:
					escribirFichero(nameFile);
					break;
			}
			
		}while(opcion != 6);
		
		
		

	}

}

