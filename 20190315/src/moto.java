
public class moto {
	
	//ATRIBUTOS CLASE MOTO
	String matricula;
	String marca;
	String modelo;
	double precio;
	int cilindradas;
	int numeroluces;
	boolean radio;
	
	//METODO CONSTRUCTOR PARA CREAR OBJETOS MOTO
	public moto(String matricula, String marca, String modelo, double precio, int cilindradas, int numeroluces,
			boolean radio) {
		this.matricula = matricula;
		this.marca = marca;
		this.modelo = modelo;
		this.precio = precio;
		this.cilindradas = cilindradas;
		this.numeroluces = numeroluces;
		this.radio = radio;
	}

	
	
	
	//METODOS GET & SET PARA OBTENER 
	public String getMatricula() {
		return matricula;
	}

	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public double getPrecio() {
		return precio;
	}

	public void setPrecio(double precio) {
		this.precio = precio;
	}

	public int getCilindradas() {
		return cilindradas;
	}

	public void setCilindradas(int cilindradas) {
		this.cilindradas = cilindradas;
	}

	public int getNumeroluces() {
		return numeroluces;
	}

	public void setNumeroluces(int numeroluces) {
		this.numeroluces = numeroluces;
	}

	public boolean isRadio() {
		return radio;
	}

	public void setRadio(boolean radio) {
		this.radio = radio;
	}



	//METODO TOSTRING
	@Override
	public String toString() {
		return "moto [matricula=" + matricula + ", marca=" + marca + ", modelo=" + modelo + ", precio=" + precio
				+ ", cilindradas=" + cilindradas + ", numeroluces=" + numeroluces + ", radio=" + radio + "]";
	}
	
	




}
