
public class javaPrincipal2 {

	public static void main(String[] args) {

		coche coche1 = new coche("1234VH", "Volvo", "560", 25000, 'D', 123, 5, false);
		coche coche2 = new coche("1234VH", "Mercedes", "560", 25000, 'G', 123, 5, false);
		coche coche3 = new coche("1234VH", "Ferrari", "560", 25000, 'G', 123, 5, false);
		coche coche4 = new coche("1234VH", "Kia", "560", 25000, 'H', 123, 5, false);
		coche coche5 = new coche("1234VH", "Renault", "560", 25000, 'D', 123, 5, false);
		coche coche6 = new coche("1234VH", "Suzuki", "560", 25000, 'D', 123, 5, false);
		coche coche7 = new coche("1234VH", "Suzuki", "560", 25000, 'D', 123, 5, false);
		coche coche8 = new coche("1234VH", "Suzuki", "560", 25000, 'D', 123, 5, false);

		coche [][] arrayCoches = new coche[2][4];
		arrayCoches[1][1] = coche1;
		arrayCoches[0][3] = coche2;
		arrayCoches[0][0] = coche3;
		arrayCoches[1][2] = coche4;
		arrayCoches[0][1] = coche5;
		arrayCoches[1][0] = coche6;
		arrayCoches[0][2] = coche7;
		arrayCoches[1][3] = coche8;
		
		
		for(int i=0; i<arrayCoches.length; i++) {
			for(int j=0; j<arrayCoches[i].length; j++) {
				System.out.print(arrayCoches[i][j].getMarca() + arrayCoches[i][j].getModelo() + "\t");
			} 
			System.out.println();
		}
		
		System.out.println(coche1.CalculoIVA());
		System.out.println(coche1.Categoria());
		coche1.pincharRueda();
		System.out.println(coche1.getRuedas());
		
	}
}
