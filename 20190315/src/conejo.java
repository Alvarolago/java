
public class conejo extends Animales{
	double tama�o;
	String pelo;
	boolean campero;
	public conejo(String nombre, int edad, boolean chip, double peso, String color, double tama�o, String pelo,
			boolean campero) {
		super(nombre, edad, chip, peso, color);
		this.tama�o = tama�o;
		this.pelo = pelo;
		this.campero = campero;
	}
	public double getTama�o() {
		return tama�o;
	}
	public void setTama�o(double tama�o) {
		this.tama�o = tama�o;
	}
	public String getPelo() {
		return pelo;
	}
	public void setPelo(String pelo) {
		this.pelo = pelo;
	}
	public boolean isCampero() {
		return campero;
	}
	public void setCampero(boolean campero) {
		this.campero = campero;
	}
	@Override
	public String toString() {
		return "conejo [tama�o=" + tama�o + ", pelo=" + pelo + ", campero=" + campero + "]";
	}
	

}
