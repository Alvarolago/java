
public class camion {

	//ATRIBUTOS DE LA CLASE
	String matricula;
	String marca;
	String modelo;
	double precio;
	double peso;
	int numeroejes;
	boolean remolque;
	double capacidad;
	char se�al; //lo que transporta
	
	
	public camion(String matricula, String marca, String modelo, double precio, double peso, int numeroejes,
			boolean remolque, double capacidad, char se�al) {
		this.matricula = matricula;
		this.marca = marca;
		this.modelo = modelo;
		this.precio = precio;
		this.peso = peso;
		this.numeroejes = numeroejes;
		this.remolque = remolque;
		this.capacidad = capacidad;
		this.se�al = se�al;
	}


	public String getMatricula() {
		return matricula;
	}


	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}


	public String getMarca() {
		return marca;
	}


	public void setMarca(String marca) {
		this.marca = marca;
	}


	public String getModelo() {
		return modelo;
	}


	public void setModelo(String modelo) {
		this.modelo = modelo;
	}


	public double getPrecio() {
		return precio;
	}


	public void setPrecio(double precio) {
		this.precio = precio;
	}


	public double getPeso() {
		return peso;
	}


	public void setPeso(double peso) {
		this.peso = peso;
	}


	public int getNumeroejes() {
		return numeroejes;
	}


	public void setNumeroejes(int numeroejes) {
		this.numeroejes = numeroejes;
	}


	public boolean isRemolque() {
		return remolque;
	}


	public void setRemolque(boolean remolque) {
		this.remolque = remolque;
	}


	public double getCapacidad() {
		return capacidad;
	}


	public void setCapacidad(double capacidad) {
		this.capacidad = capacidad;
	}


	public char getSe�al() {
		return se�al;
	}


	public void setSe�al(char se�al) {
		this.se�al = se�al;
	}


	@Override
	public String toString() {
		return "camion [matricula=" + matricula + ", marca=" + marca + ", modelo=" + modelo + ", precio=" + precio
				+ ", peso=" + peso + ", numeroejes=" + numeroejes + ", remolque=" + remolque + ", capacidad="
				+ capacidad + ", se�al=" + se�al + "]";
	}
	
	

}
