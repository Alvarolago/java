
public class perro extends Animales {

	String raza;
	boolean ladra;
	String tipo;
	public perro(String nombre, int edad, boolean chip, double peso, String color, String raza, boolean ladra,
			String tipo) {
		super(nombre, edad, chip, peso, color);
		this.raza = raza;
		this.ladra = ladra;
		this.tipo = tipo;
	}
	public String getRaza() {
		return raza;
	}
	public void setRaza(String raza) {
		this.raza = raza;
	}
	public boolean isLadra() {
		return ladra;
	}
	public void setLadra(boolean ladra) {
		this.ladra = ladra;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	@Override
	public String toString() {
		return "perro [raza=" + raza + ", ladra=" + ladra + ", tipo=" + tipo + "]";
	}

	
	
}
