
public class trapecio extends figura{

	double bMayor;
	double bMenor;
	double altura;
	
	public trapecio(String color, char tama�o, double bMayor, double bMenor, double altura) {
		super(color, tama�o);
		this.bMayor = bMayor;
		this.bMenor = bMenor;
		this.altura = altura;
	}

	public double getbMayor() {
		return bMayor;
	}

	public void setbMayor(double bMayor) {
		this.bMayor = bMayor;
	}

	public double getbMenor() {
		return bMenor;
	}

	public void setbMenor(double bMenor) {
		this.bMenor = bMenor;
	}

	public double getAltura() {
		return altura;
	}

	public void setAltura(double altura) {
		this.altura = altura;
	}

	@Override
	public String toString() {
		return "trapecio [color=" + color + ", tama�o=" + tama�o + ", bMayor=" + bMayor + ", bMenor=" + bMenor
				+ ", altura=" + altura + "]";
	}
	public double calcularArea() {
		double area;
		area=altura*((bMayor+bMenor)/2);
		return area;

	}	
}
