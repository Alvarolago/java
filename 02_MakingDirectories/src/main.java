import java.io.File;
import java.io.IOException;

public class main {
	public static void main(String[] args) {
		File cbin = new File("bin"); 	cbin.mkdir();
		File src = new File("src");  	src.mkdir();
		File cdoc = new File("doc"); 	cdoc.mkdir();
		
		if(createCarpet(cbin)) {
			File cbcode = new File("byteCode");		cbcode.mkdir();
			File cobj = new File("objetos"); 		cobj.mkdir();
		}
		
		if(createCarpet(src)) {
			File cclass = new File("clases");	cclass.mkdir();
			if(createCarpet(cclass)) {
				File uj = new File("uno.java"); 
				try { uj.createNewFile(); }
				catch (IOException e) {e.printStackTrace(); System.out.println(e);}
			}
		}
		
		if(createCarpet(cdoc)) {
			File chtml = new File("HTML");		chtml.mkdir();
			File cpdf = new File("pdf");		cpdf.mkdir();
			if(createCarpet(chtml)) {
				File rhtml = new File("readHTML");
				try { rhtml.createNewFile();}
				catch (IOException e) { e.printStackTrace(); System.out.println(e);}
			}else { System.out.println("");}
		}
	
		
	}
	
	
	//Funcion comprueba si existe
	public static boolean createCarpet(File file) {
		if (!file.exists()) {
			return true;
		}
		return false;
	}

}
