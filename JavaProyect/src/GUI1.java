import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import java.awt.BorderLayout;
import javax.swing.SwingConstants;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import javax.swing.DropMode;
import javax.swing.SpringLayout;
import net.miginfocom.swing.MigLayout;
import javax.swing.JButton;
import javax.swing.JTextPane;
import java.awt.event.ActionListener;
import java.math.RoundingMode;
import java.awt.event.ActionEvent;
import java.awt.Color;
import java.awt.Component;

public class GUI1 {

	private JFrame frame;
	private JTextField textField;
	private double cuota, resultado, resultado1, resultado2, resultado3, resultadoTotal;
	private double cuota1,cuota2,cuota3;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	
	
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GUI1 window = new GUI1();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public GUI1() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(Color.LIGHT_GRAY);
		frame.setBounds(100, 100, 562, 424);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JTextPane textPane = new JTextPane();
		textPane.setEnabled(false);
		textPane.setBounds(33, 53, 111, 20);
		frame.getContentPane().add(textPane);
		
		textField = new JTextField();
		textField.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				cuota = Integer.parseInt(textField.getText());
				resultado = 1/cuota;
				String cadena = String.valueOf(resultado);
				textPane.setText(cadena);
				
				
			}
		});
		textField.setBounds(33, 21, 111, 20);
		frame.getContentPane().add(textField);
		textField.setColumns(10);
		
		JButton btnConversion = new JButton("Conversion");
		btnConversion.setBounds(154, 20, 101, 23);
		frame.getContentPane().add(btnConversion);
		
		textField_1 = new JTextField();
		textField_1.setBounds(33, 144, 51, 20);
		frame.getContentPane().add(textField_1);
		textField_1.setColumns(10);
		
		textField_2 = new JTextField();
		textField_2.setColumns(10);
		textField_2.setBounds(94, 144, 51, 20);
		frame.getContentPane().add(textField_2);
		
		textField_3 = new JTextField();
		textField_3.setColumns(10);
		textField_3.setBounds(154, 144, 51, 20);
		frame.getContentPane().add(textField_3);
		
		JTextPane textPane_1 = new JTextPane();
		textPane_1.setBounds(33, 175, 51, 20);
		frame.getContentPane().add(textPane_1);
		
		
		
		JButton btnClick = new JButton("Click");
		btnClick.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cuota1 = Integer.parseInt(textField_1.getText());
				resultado1 = suma.calculo(3,2);
				String Cadena1 = String.valueOf(resultado1);
				textPane_1.setText(Cadena1);
			}
		});
		btnClick.setBounds(215, 143, 89, 23);
		frame.getContentPane().add(btnClick);
		
		
		JTextPane textPane_2 = new JTextPane();
		textPane_2.setBounds(94, 175, 51, 20);
		frame.getContentPane().add(textPane_2);
		
		JTextPane textPane_3 = new JTextPane();
		textPane_3.setBounds(154, 175, 51, 20);
		frame.getContentPane().add(textPane_3);
		
		JTextPane textPane_4 = new JTextPane();
		textPane_4.setBounds(215, 175, 51, 20);
		frame.getContentPane().add(textPane_4);
		
		JButton btnExit = new JButton("EXIT");
		btnExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int respuesta = JOptionPane.showConfirmDialog(null, "Desea Salir?", "Confirmar salida",JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
				
				if(respuesta == 0)
					System.exit(0);
			}
		});
		btnExit.setBounds(151, 266, 89, 23);
		frame.getContentPane().add(btnExit);
		
	}
}
