import javax.swing.JFrame;
import javax.swing.JButton;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class hija {

	private JFrame frmHija;
	public JFrame getFrmHija() {
		return frmHija;
	}

	public void setFrmHija(JFrame frmHija) {
		this.frmHija = frmHija;
	}

	Ejemplo padre;

	/**
	 * Create the application.
	 */
	public hija(Ejemplo padre) {
		initialize();
		this.padre=padre;
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmHija = new JFrame();
		frmHija.setTitle("Hija");
		frmHija.setBounds(100, 100, 740, 454);
		frmHija.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmHija.getContentPane().setLayout(null);
		
		JButton button = new JButton("<<IR A PADRE");
		button.setFont(new Font("Verdana", Font.BOLD, 20));
		button.setBounds(213, 153, 283, 72);
		frmHija.getContentPane().add(button);
		
		//Eventos
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				padre.getFrmEjemplo().setVisible(true);
				frmHija.setVisible(false);
			}
		});
		
	}

}
