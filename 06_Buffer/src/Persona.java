
public class Persona {
	
	private String nombre;
	private int edad;
	
	public Persona() {
		nombre = "";
		edad = 0;
	}
	
	public Persona(String n, int e) {
		nombre = n;
		edad = e;
	}
	
	
	/*GETTERS AND SETTERS
	 ------------------*/
	public String getName() {
		return nombre;
	}
	
	public int getEdad() {
		return edad;
	}
	
	public void setNombre(String n) {
		nombre = n;
	}
	
	public void setEdad(int e) {
		edad = e;
	}

}
