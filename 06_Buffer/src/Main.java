
public class Main {
	

	
	
	public static void mostrarPantalla(Persona[] tabla) {
		for(int cnt=0; cnt<tabla.length; cnt++) {
			System.out.print  ("Nombre: " + tabla[cnt].getName());
			System.out.println("Edad: " + tabla[cnt].getEdad());

		}
		
	}
	
	public static void vaciarTabla(Persona[] tabla) {
		for(int cnt=0; cnt<tabla.length; cnt++) {
			//1� opcion : Cambiando los valores del objeto
			tabla[cnt].setNombre("");
			tabla[cnt].setEdad(0);
			//2� opcion : Creando objetos nuevos
			tabla[cnt] = new Persona();
		}
	}
	
	

	public static void main(String[] args) {
		String[] miTabla = {"Juan", "Ana", "Pedro", "Laura"};
		String[] otraTabla = new String[10];
		
		FicherosBuffer fich = new FicherosBuffer("fichero.txt");
		
		fich.EscribirTabla(miTabla);
		fich.LeerTabla(otraTabla);
		
		
		for(int cnt=0; cnt < otraTabla.length; cnt++) {
			System.out.println("Pos: " + cnt + " " + otraTabla[cnt]);
		
		}
		
		//TABLA PERSONAS
		Persona[] miPersona = new Persona[5];
		
		miPersona[0] = new Persona("Juan", 19);
		miPersona[1] = new Persona("AnaIsabel", 12);
		miPersona[2] = new Persona("Lorenzo", 23);
		miPersona[3] = new Persona("Lorenzo", 22);
		miPersona[4] = new Persona("Lorenzo", 15);

		
		
		//Crear metodo para imprimir la tabla clase persona
		mostrarPantalla(miPersona);
		
		
		FicheroAlumnos fa = new FicheroAlumnos("falumnos.txt");		//Como no es static debemos crear un objeto de la clase
		fa.EscribirTabla(miPersona);
		
		vaciarTabla(miPersona);
		mostrarPantalla(miPersona);
		fa.LeerTabla(miPersona);
		mostrarPantalla(miPersona);
		
		fich.OrdenarTabla(miPersona);
		
		
		
		/*- Escribir tabla en el fichero ordenada por edad
		  - Escribir tabla en el fichero ordenada por nombre*/
		

	}

}


