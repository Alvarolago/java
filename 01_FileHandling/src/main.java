
import java.io.*;


public class main {
	
	//INICIALIZAMOS FILE

	public static void main(String[] args) {
		//Carpet
		File carpeta = new File("CARPETA");
		//Files
		File archivo1 = new File(carpeta, "primero.txt"), 
				 archivo2 = new File(carpeta, "segundo.txt");
		
		if( carpeta.mkdir() ) {
			System.out.println("Se ha creado la carpeta");
			
			existe(archivo1);
			existe(archivo2);
		}else {
			System.out.println("La carpeta esta creada");
			if(carpeta.exists()) {
				existe(archivo1);
				existe(archivo2);
			}
		}
		
	}
	
	
		
	//CREATING FILE
	public static void existe(File file) {		
		if (!file.exists()) {						//To create a new file if does not exists
			try {
				file.createNewFile();				//Creating new file
				System.out.println(file.getName() + " ha sido creado");
			}catch(IOException ex) {ex.printStackTrace(); System.out.println(ex.getMessage());}
		}
	}
	


}
