import java.io.*;

public class FicherosFileWritter {
	
	
	public static void leerFichero(String nombre, String[] nombres) {
		try {
			
			//1 Apertura de fichero
			File fichero = new File(nombre);
			FileReader fr = new FileReader(fichero);
			
			//2 Lectura del fichero
			int letra;
			String cadena = "";
			int posicion = 0;
			
			letra = fr.read();
			while (letra != -1) {
				
				if(letra == ';') {
					nombres[posicion] = cadena;
					cadena="";
					posicion++;
				}else {
					//Tratamiento del caracter leido
					cadena += ((char) letra);
				}

				//Leer el siguiente caracter
				letra = fr.read();

			}
			//Cerramos el fichero
			fr.close();
			
			//Escribir la cadena por pantalla
			System.out.println("He leido: " + cadena);
			
			
		} catch (FileNotFoundException e) { e.printStackTrace(); System.out.println(e.getMessage()); 
		} catch (IOException e) 		  { e.printStackTrace(); System.out.println(e.getMessage());}
		
		
	}
	
	
	public static void leerFicheroSalto(String nombre, String[] nombres) {
		try {
			
			//1 Apertura de fichero
			File fichero = new File(nombre);
			FileReader fr = new FileReader(fichero);
			
			//2 Lectura del fichero
			int letra;
			String cadena = "";
			int posicion = 0;
			
			letra = fr.read();
			while (letra != -1) {
				
				if(letra == '\n') {
					nombres[posicion] = cadena;
					cadena="";
					posicion++;
				}else {
					//Tratamiento del caracter leido
					cadena += ((char) letra);
				}

				//Leer el siguiente caracter
				letra = fr.read();

			}
			//Cerramos el fichero
			fr.close();
			
			//Escribir la cadena por pantalla
			System.out.println("He leido: " + cadena);
			
			
		} catch (FileNotFoundException e) { e.printStackTrace(); System.out.println(e.getMessage()); 
		} catch (IOException e) 		  { e.printStackTrace(); System.out.println(e.getMessage());}
		
		
	}
		
	
	public static void escribirFichero(String nombre, String[] nombres) {
		
		//Apertura del fichero
		File fichero = new File("ficheroTexto1.txt");
	
		try {
			
				//Creamos el FileWriter
				FileWriter fw = new FileWriter(fichero);
				//Escribir en el fichero
				for(int cnt=0; cnt<nombres.length; cnt++) {
					fw.write(nombres[cnt] + ";");
				}
				
				//Cerrar Fichero
				fw.close();
				
			
		} catch (IOException e) { e.printStackTrace(); System.out.println(e.getMessage());}	
		
	}
	
	
	public static void escribirFicheroSalto(String nombre, String[] nombres) {
		
		//Apertura del fichero
		File fichero = new File("ficheroTexto1.txt");
	
		try {
			
				//Creamos el FileWriter
				FileWriter fw = new FileWriter(fichero);
				//Escribir en el fichero
				for(int cnt=0; cnt<nombres.length; cnt++) {
					fw.write(nombres[cnt] + "\n");
				}
				
				//Cerrar Fichero
				fw.close();
				
			
		} catch (IOException e) { e.printStackTrace(); System.out.println(e.getMessage());}	
		
	}
	
	public static void escribirFicheroCadaCuatro(String nombre, String[] nombres) {
		
		//Apertura del fichero
		File fichero = new File("ficheroTexto1.txt");
	
		try {
			
				//Creamos el FileWriter
				FileWriter fw = new FileWriter(fichero);
				//Escribir en el fichero
				
				for(int cnt=0; cnt<nombres.length; cnt++) {
					fw.write(nombres[cnt]);
					if(cnt%4 == 1) {
						 fw.write("\n");
					}
				}
				
				//Cerrar Fichero
				fw.close();
				
			
		} catch (IOException e) { e.printStackTrace(); System.out.println(e.getMessage());}	
		
	}

	
	
	public static void inicTabla(String[] nombres) {
		for(int cnt=0; cnt<nombres.length;cnt++)
			nombres[cnt] = "";
	}
	
	public static void escribirTabla(String[] nombres) {
		System.out.println("Contenido de la tabla es:");
		for(int cnt=0; cnt<nombres.length; cnt++)
			System.out.print(nombres[cnt] + " ");
	}
	

	public static void main(String[] args) {
		String[] listaNombres = {"Pepe", "Laura", "Juan", "Ana"};
		
		//escribirFichero("FicheroTexto1.txt", listaNombres);
		//escribirFicheroSalto("FicheroTexto1.txt", listaNombres);
		escribirFicheroCadaCuatro("FicheroTexto1.txt", listaNombres);
		
		inicTabla(listaNombres);
		escribirTabla(listaNombres);
		
		//leerFichero("FicheroTexto1.txt", listaNombres);
		//leerFicheroSalto("FicheroTexto1.txt", listaNombres);
		escribirTabla(listaNombres);
		
		
		

	}

}
