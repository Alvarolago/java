import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.FlowLayout;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JRadioButton;
import javax.swing.JCheckBox;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JButton;
import javax.swing.ButtonGroup;
import javax.swing.AbstractAction;
import java.awt.event.ActionEvent;
import javax.swing.Action;
import java.awt.event.ActionListener;
import javax.swing.JTextPane;
import javax.swing.JScrollPane;
import java.awt.Component;
import javax.swing.ScrollPaneConstants;
import java.awt.event.ContainerAdapter;
import java.awt.event.ContainerEvent;

public class AlvaroLagoLopez {

	private JFrame frmRegistroDeUsuario;
	private JTextField textField;
	private JTextField textField_1;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private final Action action = new SwingAction();
	private final ButtonGroup buttonGroup_1 = new ButtonGroup();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AlvaroLagoLopez window = new AlvaroLagoLopez();
					window.frmRegistroDeUsuario.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public AlvaroLagoLopez() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmRegistroDeUsuario = new JFrame();
		frmRegistroDeUsuario.setResizable(false);
		frmRegistroDeUsuario.setTitle("Registro de Usuario");
		frmRegistroDeUsuario.setBounds(100, 100, 584, 471);
		frmRegistroDeUsuario.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmRegistroDeUsuario.getContentPane().setLayout(null);
		
		JLabel lblUsuario = new JLabel("Usuario:");
		lblUsuario.setBounds(45, 46, 63, 14);
		frmRegistroDeUsuario.getContentPane().add(lblUsuario);
		
		JLabel lblNewLabel = new JLabel("Password:");
		lblNewLabel.setBounds(34, 71, 63, 14);
		frmRegistroDeUsuario.getContentPane().add(lblNewLabel);
		
		textField = new JTextField();
		textField.addContainerListener(new ContainerAdapter() {

		});
		textField.setBounds(114, 43, 146, 20);
		frmRegistroDeUsuario.getContentPane().add(textField);
		textField.setColumns(10);
		
		textField_1 = new JTextField();
		textField_1.setBounds(114, 68, 146, 20);
		frmRegistroDeUsuario.getContentPane().add(textField_1);
		textField_1.setColumns(10);
		
		JButton btnNewButton = new JButton("Registrar Usuario");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(textField.getText() == null) {
					JOptionPane.showMessageDialog(null, "Registro incorrecto");
				}
				else {
					JOptionPane.showMessageDialog(null, "Registro correcto");
				}
				
			}
		});
		btnNewButton.setEnabled(false);
		btnNewButton.setBounds(116, 313, 162, 23);
		frmRegistroDeUsuario.getContentPane().add(btnNewButton);
		
		JCheckBox chckbxAceptoLosTrminos = new JCheckBox("Acepto los t\u00E9rminos");
		chckbxAceptoLosTrminos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				btnNewButton.setEnabled(true);
			}
		});
		chckbxAceptoLosTrminos.setEnabled(false);
		chckbxAceptoLosTrminos.setBounds(117, 215, 143, 23);
		frmRegistroDeUsuario.getContentPane().add(chckbxAceptoLosTrminos);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setEnabled(false);
		scrollPane.setBounds(317, 231, 172, 115);
		frmRegistroDeUsuario.getContentPane().add(scrollPane);
		
		
		JTextPane txtpnLasPresentesCondiciones = new JTextPane();
		txtpnLasPresentesCondiciones.setEditable(false);
		txtpnLasPresentesCondiciones.setAutoscrolls(false);
		scrollPane.setViewportView(txtpnLasPresentesCondiciones);
		txtpnLasPresentesCondiciones.setText("T\u00E9rminos y Condiciones del Contrato: Las presentes Condiciones Generales de contrataci\u00F3n, junto con las Condiciones Perticulares que, en cada caso y servicio, puedan establecerse (en lo sucesivo, y en conjunto, las \"Condiciones de Contrataci\u00F3n\") regulan expresamente las relaciones surguidas entre ambas partes.");
		
		JRadioButton rdbtnMenorDe = new JRadioButton("Menor de 18 a\u00F1os");
		rdbtnMenorDe.setSelected(true);
		rdbtnMenorDe.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
					chckbxAceptoLosTrminos.setEnabled(false);
					btnNewButton.setEnabled(false);
					chckbxAceptoLosTrminos.setSelected(false);
				
			}
		});
		buttonGroup.add(rdbtnMenorDe);
		rdbtnMenorDe.setBounds(114, 138, 146, 23);
		frmRegistroDeUsuario.getContentPane().add(rdbtnMenorDe);
		
		JRadioButton rdbtnMayorDe = new JRadioButton("Mayor de 18 a\u00F1os");
		rdbtnMayorDe.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				chckbxAceptoLosTrminos.setEnabled(true);
				//btnNewButton.setEnabled(true);
			}
		});
		buttonGroup.add(rdbtnMayorDe);
		rdbtnMayorDe.setBounds(114, 164, 135, 23);
		frmRegistroDeUsuario.getContentPane().add(rdbtnMayorDe);
		
		JRadioButton rdbtnVerTrminos = new JRadioButton("Ver t\u00E9rminos");
		rdbtnVerTrminos.setSelected(true);
		rdbtnVerTrminos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				txtpnLasPresentesCondiciones.setVisible(true);
				scrollPane.setVisible(true);
			}
		});
		buttonGroup_1.add(rdbtnVerTrminos);
		rdbtnVerTrminos.setBounds(291, 138, 109, 23);
		frmRegistroDeUsuario.getContentPane().add(rdbtnVerTrminos);
		
		JRadioButton rdbtnOcultarTrminos = new JRadioButton("Ocultar t\u00E9rminos");
		rdbtnOcultarTrminos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				txtpnLasPresentesCondiciones.setVisible(false);
				scrollPane.setVisible(false);
			}
		});
		buttonGroup_1.add(rdbtnOcultarTrminos);
		rdbtnOcultarTrminos.setBounds(291, 164, 109, 23);
		frmRegistroDeUsuario.getContentPane().add(rdbtnOcultarTrminos);
		
	}
	private class SwingAction extends AbstractAction {
		public SwingAction() {
			putValue(NAME, "SwingAction");
			putValue(SHORT_DESCRIPTION, "Some short description");
		}
		public void actionPerformed(ActionEvent e) {
		}
	}
}
