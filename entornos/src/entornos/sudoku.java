package entornos;

public class sudoku
	 {
	     // instance variables - replace the example below with your own
	     int numero;
	     int colrandom;
	     int filarandom;
	     private int numColumnas;	//numero de columnas que tiene el juego
	     private int numFilas;	//numero de filas que tiene el juego
	     private int matriz[][];	//lugar en el que se almacenan los datos
	     
	     /**
	      * Constructor for objects of class sudoku
	      */
	     public sudoku()
	     {
	         // initialise instance variables
	         numColumnas = 9;
	         numFilas = 9;
	         matriz = new int[numFilas][numColumnas];
	         
	     }

	         
	     public void RellenoSudoku(sudoku sudoku) {
	    	 int fila;
	    	 int columna;
	    	 for(int filas = 0; filas < numFilas; filas++)
		 		{
		 			for(int columnas = 0; columnas < numColumnas; columnas++)
		 			{
		 				matriz[filas][columnas] = 0 ;
		 			}
		 		}
	    	 for (int r=0; r<45; r++) {
	    		 numero = (int) (Math.random() * 9) + 1;
	    		 filarandom = (int) (Math.random() * 9);
	    		 colrandom = (int) (Math.random() * 9);
	    		 matriz[filarandom][colrandom] = numero;	 
	    	 }
	     }
	     public void TableroSudoku(sudoku sudoku)
	 	{
	    	 System.out.println("JUEGO DE SUDOKU");
	         System.out.println();
	         System.out.println("----------------------------------------------");
	 		for(int fila = 0; fila < numFilas; fila++)
	 		{
	 			for(int columna = 0; columna < numColumnas; columna++)
	 			{
	 				System.out.print("|");
	 				System.out.print(matriz[fila][columna] + " | ");
	 			}
	 			System.out.println();
	 			System.out.println(" ----------------------------------------------");
	 		}
	 	}
}

