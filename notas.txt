﻿1. Crear la clase.
2. Añadir los atributos de la clase (String, int, double, char,...)
3. Crear método constructor.
	3.1 Se llama igual que la clase.
	3.2 Se le pasa por parámetro los atributos de la clase.
	3.3 Se asocia cada atributo con su correspondiente.
	3.4 Va a permitir crear objetos y en el orden de los atributos fijados.
4. Crear métodos GET & SET.
	4.1 El método GET retorna el valor de un atributo.
	4.2 El método SET modifica el valor de un atributo.
5. Crear el método tostring.
	5.1 El método toString permite sacar por pantalla el objeto.

Ejercicio: Crear 2 clases, una la clase Alumno y otra la clase Equipo. Cada clase debe tener todos los atributos que hemos visto en el aula. Crear un objeto de cada clase. Y "jugar" con los metodos tostring, get & set.