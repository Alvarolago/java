
public class circulo extends figura{

	double radio;

	public circulo(String color, char tama�o, double radio) {
		super(color, tama�o);
		this.radio = radio;
	}

	public double getRadio() {
		return radio;
	}

	public void setRadio(double radio) {
		this.radio = radio;
	}

	@Override
	public String toString() {
		return "circulo [color=" + color + ", tama�o=" + tama�o + ", radio=" + radio + "]";
	}
	
	public double calcularArea() {
		double area;
		area=radio*radio*3.14;
		return area;
	}
}
