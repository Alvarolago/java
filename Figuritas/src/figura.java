
public class figura {

	String color;
	char tama�o;
	
	public figura(String color, char tama�o) {
		super();
		this.color = color;
		this.tama�o = tama�o;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public char getTama�o() {
		return tama�o;
	}

	public void setTama�o(char tama�o) {
		this.tama�o = tama�o;
	}

	@Override
	public String toString() {
		return "figura [color=" + color + ", tama�o=" + tama�o + "]";
	}
	
	
}
