import java.util.Scanner;

public class principalfiguras {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int n=0;
		do {
			System.out.println(" Men� para calcular el area de las figuras:");
			System.out.println("--------------------------------------------");
			System.out.println("1.Para calcular area del circulo");
			System.out.println("2.Para calcular area del cuadrado");
			System.out.println("3.Para calcular area del triangulo");
			System.out.println("4.Para calcular area del rombo");
			System.out.println("5.Para calcular area del trapecio");
			System.out.println("6.Para calcular area del rectangulo");
			System.out.println("7.Para salir");
			System.out.print("Elija una opci�n: ");
			Scanner teclado = new Scanner(System.in);
			n = teclado.nextInt();
			
			switch (n) {
				case 1: System.out.println("Calcular area circulo:\n");
						System.out.println("Introduzca el radio");
						double radio = teclado.nextDouble();
						circulo cir = new circulo("Gris", 'G', radio);
						System.out.println("El area es: " + cir.calcularArea());
						break;
						
				case 2: System.out.println("Calcular area cuadrado:\n");
						System.out.println("Introduzca el lado");
						double lado = teclado.nextDouble();
						cuadrado cua = new cuadrado("Gris", 'G', lado);
						System.out.println("El area es: " + cua.calcularArea());
						break;
						
				case 3: System.out.println("Calcular area triangulo\n");
						System.out.println("Introduzca la base");
						double base = teclado.nextDouble();
						System.out.println("Introduzca la altura");
						double altura = teclado.nextDouble();
						triangulo tri = new triangulo("Gris", 'G', base, altura);
						System.out.println("El area es: " + tri.calcularArea());
						break;
						
				case 4: System.out.println("Calcular area rombo:\n");
						System.out.println("Introduzca la diagonal menor");
						double dMenor = teclado.nextDouble();
						System.out.println("Introduzca la diagonal mayor");
						double dMayor = teclado.nextDouble();
						rombo rombo = new rombo("Gris", 'G', dMenor, dMayor);
						System.out.println("El area es: " + rombo.calcularArea());				
						break;
						
				case 5: System.out.println("Calcular area trapecio:\n");
						System.out.println("Introduzca la base mayor");
						double bMayor = teclado.nextDouble();
						System.out.println("Introduzca la base menor");
						double bMenor = teclado.nextDouble();
						System.out.println("Introduzca la altura");
						double alt = teclado.nextDouble();
						trapecio tra = new trapecio("Gris", 'G', bMayor, bMenor, alt);
						System.out.println("El area es: " + tra.calcularArea());
						break;
						
				case 6: System.out.println("Calcular area rectangulo:\n");
						System.out.println("Introduzca la base");
						double ba = teclado.nextDouble();
						System.out.println("Introduzca la altura");
						double al = teclado.nextDouble();
						rectangulo rec = new rectangulo("Gris", 'G', ba, al);
						System.out.println("El area es: " + rec.calcularArea());
						break;
						
				case 7: System.out.println("Bye bye");
						break;
						
				default: System.out.println("Opci�n no v�lida");
			}
			
		}while (n != 7);
	}

}
