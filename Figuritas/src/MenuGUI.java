import java.awt.EventQueue;
import java.util.Scanner;

import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;


public class MenuGUI {

	private JFrame frmCalcularAreas;
	private JTextField Texto;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private int variable_1, variable_2, variable_3;
	private double resultado;
	Scanner teclado = new Scanner(System.in);
	private JTextField Texto2;
	private JTextField Texto3;
	private JTextField PanelTexto;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MenuGUI window = new MenuGUI();
					window.frmCalcularAreas.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public MenuGUI() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmCalcularAreas = new JFrame();
		frmCalcularAreas.setResizable(false);
		frmCalcularAreas.setTitle("Calcular Areas");
		frmCalcularAreas.setBounds(100, 100, 786, 504);
		frmCalcularAreas.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmCalcularAreas.getContentPane().setLayout(null);
		
		Texto = new JTextField();
		Texto.setBounds(349, 196, 86, 20);
		frmCalcularAreas.getContentPane().add(Texto);
		Texto.setColumns(10);
		
		JRadioButton Cuadrado = new JRadioButton("Cuadrado");
		Cuadrado.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Texto2.setEnabled(false);
				Texto3.setEnabled(false);
				JOptionPane.showMessageDialog(null, "Debe introducir el lado en el 1er Campo");
			}
		});
		buttonGroup.add(Cuadrado);
		Cuadrado.setBounds(43, 193, 109, 23);
		frmCalcularAreas.getContentPane().add(Cuadrado);
		
		JRadioButton Circulo = new JRadioButton("Circulo");
		Circulo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Texto2.setEnabled(false);
				Texto3.setEnabled(false);
				JOptionPane.showMessageDialog(null, "Debe introducir el radio en el 1er Campo");
			}
		});
		buttonGroup.add(Circulo);
		Circulo.setBounds(43, 219, 109, 23);
		frmCalcularAreas.getContentPane().add(Circulo);
		
		JRadioButton Triangulo = new JRadioButton("Triangulo");
		Triangulo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
					Texto2.setEnabled(true);
					Texto3.setEnabled(false);
					JOptionPane.showMessageDialog(null, "Debe introcducir la base en el 1er Campo y la altura en el 2� campo");
			}
		});
		buttonGroup.add(Triangulo);
		Triangulo.setBounds(43, 245, 109, 23);
		frmCalcularAreas.getContentPane().add(Triangulo);
		
		JRadioButton Rombo = new JRadioButton("Rombo");
		Rombo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Texto2.setEnabled(true);
				Texto3.setEnabled(false);
				JOptionPane.showMessageDialog(null, "Debe introducir la diagonal MAYOR en el 1er Campo y la diagonal MENOR en el 2� campo");
			}
		});
		buttonGroup.add(Rombo);
		Rombo.setBounds(43, 271, 109, 23);
		frmCalcularAreas.getContentPane().add(Rombo);
		
		JRadioButton Rectangulo = new JRadioButton("Rectangulo");
		Rectangulo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Texto2.setEnabled(true);
				Texto3.setEnabled(false);
				JOptionPane.showMessageDialog(null, "Debe introducir largo en el 1er Campo y el ancho en el 2� campo");
			}
		});
		buttonGroup.add(Rectangulo);
		Rectangulo.setBounds(43, 293, 109, 23);
		frmCalcularAreas.getContentPane().add(Rectangulo);
		
		JRadioButton Trapecio = new JRadioButton("Trapecio");
		Trapecio.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Texto2.setEnabled(true);
				Texto3.setEnabled(true);
				JOptionPane.showMessageDialog(null, "Debe introducir la base MAYOR 1er Campo, la base MENOR en el 2� campo y la altura en el 3� campo");
			}
		});
		buttonGroup.add(Trapecio);
		Trapecio.setBounds(43, 319, 109, 23);
		frmCalcularAreas.getContentPane().add(Trapecio);
		
		JLabel lblCalcularAreas = new JLabel("CALCULAR AREAS");
		lblCalcularAreas.setFont(new Font("Segoe Print", Font.PLAIN, 43));
		lblCalcularAreas.setBounds(196, 11, 500, 87);
		frmCalcularAreas.getContentPane().add(lblCalcularAreas);
		
		JLabel lblSeleccionaFigura = new JLabel("Selecciona figura");
		lblSeleccionaFigura.setFont(new Font("Tekton Pro Ext", Font.BOLD, 15));
		lblSeleccionaFigura.setBounds(46, 156, 185, 26);
		frmCalcularAreas.getContentPane().add(lblSeleccionaFigura);
		
		JButton btnSend = new JButton("Calcular");
		btnSend.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				if(Cuadrado.isSelected()) {
					variable_1 = Integer.parseInt(Texto.getText());
					cuadrado cua = new cuadrado("Gris", 'G', variable_1);
					resultado = cua.calcularArea();
					String res = String.valueOf(resultado);
					PanelTexto.setText(res);
				}
				if(Circulo.isSelected()) {
					variable_1 = Integer.parseInt(Texto.getText());
					circulo cir = new circulo("Gris", 'G', variable_1);
					resultado = cir.calcularArea();
					String res = String.valueOf(resultado);
					PanelTexto.setText(res);
				}
				if(Triangulo.isSelected()) {
					variable_1 = Integer.parseInt(Texto.getText());
					variable_2 = Integer.parseInt(Texto2.getText());
					triangulo tri = new triangulo("Gris", 'G', variable_1, variable_2);
					resultado = tri.calcularArea();
					String res = String.valueOf(resultado);
					PanelTexto.setText(res);
				}
				if(Rombo.isSelected()) {
					variable_1 = Integer.parseInt(Texto.getText());
					variable_2 = Integer.parseInt(Texto2.getText());
					rombo rombo = new rombo("Gris", 'G', variable_1, variable_2);
					resultado = rombo.calcularArea();
					String res = String.valueOf(resultado);
					PanelTexto.setText(res);
				}
				if(Trapecio.isSelected()) {
					variable_1 = Integer.parseInt(Texto.getText());
					variable_2 = Integer.parseInt(Texto2.getText());
					variable_3 = Integer.parseInt(Texto3.getText());
					trapecio tra = new trapecio("Gris", 'G', variable_1, variable_2, variable_3);
					resultado = tra.calcularArea();
					String res = String.valueOf(resultado);
					PanelTexto.setText(res);
				}
				if(Rectangulo.isSelected()) {
					variable_1 = Integer.parseInt(Texto.getText());
					variable_2 = Integer.parseInt(Texto2.getText());
					rectangulo rec = new rectangulo("Gris", 'G', variable_1, variable_2);
					resultado = rec.calcularArea();
					String res = String.valueOf(resultado);
					PanelTexto.setText(res);
					
				}
				

			}
		});
		btnSend.setBounds(445, 193, 86, 75);
		frmCalcularAreas.getContentPane().add(btnSend);
		
		Texto2 = new JTextField();
		Texto2.setBounds(349, 222, 86, 20);
		frmCalcularAreas.getContentPane().add(Texto2);
		Texto2.setColumns(10);
		
		Texto3 = new JTextField();
		Texto3.setBounds(349, 248, 86, 20);
		frmCalcularAreas.getContentPane().add(Texto3);
		Texto3.setColumns(10);
		
		PanelTexto = new JTextField();
		PanelTexto.setFont(new Font("Tahoma", Font.PLAIN, 45));
		PanelTexto.setEditable(false);
		PanelTexto.setEnabled(false);
		PanelTexto.setBounds(601, 193, 109, 74);
		frmCalcularAreas.getContentPane().add(PanelTexto);
		PanelTexto.setColumns(10);
		
		JLabel lblCampo = new JLabel("Campo1:");
		lblCampo.setBounds(292, 199, 86, 14);
		frmCalcularAreas.getContentPane().add(lblCampo);
		
		JLabel lblCampo_1 = new JLabel("Campo2:");
		lblCampo_1.setBounds(292, 223, 86, 14);
		frmCalcularAreas.getContentPane().add(lblCampo_1);
		
		JLabel lblCampo_2 = new JLabel("Campo3:");
		lblCampo_2.setBounds(292, 249, 96, 14);
		frmCalcularAreas.getContentPane().add(lblCampo_2);
		
		JLabel lblIntroduccionDatos = new JLabel("Introduccion datos");
		lblIntroduccionDatos.setFont(new Font("Dialog", Font.BOLD, 15));
		lblIntroduccionDatos.setBounds(282, 159, 185, 26);
		frmCalcularAreas.getContentPane().add(lblIntroduccionDatos);
		
		JLabel lblResultado = new JLabel("Resultado");
		lblResultado.setFont(new Font("Dialog", Font.BOLD, 15));
		lblResultado.setBounds(601, 156, 185, 26);
		frmCalcularAreas.getContentPane().add(lblResultado);
		
		JButton btnNewButton = new JButton("SALIR");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int respuesta = JOptionPane.showConfirmDialog(null, "�Desea Salir?", "Confirmar Salida", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
				if(respuesta == 0) {
					frmCalcularAreas.dispose();
				}
			}
		});
		btnNewButton.setBounds(297, 395, 185, 59);
		frmCalcularAreas.getContentPane().add(btnNewButton);
	}
}
