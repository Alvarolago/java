import java.io.*;
import java.util.Scanner;

public class principal {
	
	static int personas = 4;
	
/* 0 nombre
 * 1 apellido
 * 2 edad
 */
	static String[][] data = new String[5][5];


	
	public static int menu() {
		Scanner entrada = new Scanner(System.in);
		
		System.out.println("/tMENU/t");
		System.out.println("----------------");
		System.out.println("1.-EscribirFichero");
		System.out.println("2.-LeerFichero");
		System.out.println("3.-EscribirPantalla");
		System.out.println("4.-VaciarEstructura");
		System.out.println("5.-Salir");
		
		return entrada.nextInt();
	}
	
	
	
	public static void escribirFichero(File archivo) {

			try {
				FileWriter fw = new FileWriter(archivo, true);
				for(int cnt=0; cnt<4; cnt++) {
					for(int cnt2=0; cnt2<3; cnt2++) {
						fw.write(data[cnt2][cnt]);
						fw.write(";");
					}
				fw.write("\r");
				}
				
				fw.close();
			} catch (IOException e) { e.printStackTrace(); System.out.println(e.getMessage());}
			
			

	}
	
	public static void leerFichero(File fichero) {
		try {
			FileReader fr = new FileReader(fichero);
			int letra;
			int cnt=0, cnt2=0;
			
			letra = fr.read();
			while(letra != -1 && cnt2 < 3 && cnt < 3) {
				if(letra == ';') {
					cnt2++;
				}if(letra == '\r'){
					cnt++;
				}else {
					data[cnt2][cnt] = String.valueOf(letra);
				}
				
				letra = fr.read();
			}
		}catch( IOException e) {e.printStackTrace();}
	}
	
	
	public static void comprobar(File fichero) {
		if(!fichero.exists()) {
			try {
				fichero.createNewFile();
				System.out.println("Se ha creado" + fichero.getName());
			} catch (IOException e) {e.printStackTrace();}
			
		}else {
			System.out.println("EL fichero existe");
		}
	}
	
	
	public static void mostrarTabla() {
		for(int cnt=0; cnt<4; cnt++) {
			for(int cnt2=0; cnt2<3; cnt2++) {
				System.out.println(data[cnt2][cnt]);
			}
		}
	}
	
	
	
	
	public static void main(String[] args) {

		data[0][0] = "pepe"; data[1][0] = "lope"; data[2][0] = "34";
		data[0][1] = "luis"; data[1][1] = "ruiz"; data[2][1] = "25";
		data[0][2] = "santi"; data[1][2] = "lago"; data[2][2] = "15";
		data[0][3] = "cabra"; data[1][3] = "marabilla"; data[2][3] = "30";


		int opcion = 0;
		
		File carpeta = new File("MIS FICHEROS");
		carpeta.mkdir();
		File fich = new File(carpeta, "personas.txt");
		
		comprobar(carpeta);
		comprobar(fich);
		do {
			
			System.out.println(fich.length());
			
			opcion = menu();
			switch(opcion) {
			case 1:
				escribirFichero(fich);
				break;
			case 2:
				leerFichero(fich);
				break;
			case 3:
				mostrarTabla();
				break;
			case 4:
				break;
			}
			
		}while(opcion!=5);
		

	}

}
