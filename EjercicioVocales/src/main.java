import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class main {
	
	public static String rdFrase() {
		Scanner entrada = new Scanner(System.in);
		System.out.println("Escriba la frase a continuación:\n");
		
		return entrada.nextLine();		
	}
	
	public static int[] contVocales(String fras) {
		int cnt = 0;
		int vocales[] = new int[5];
		do {
			switch(fras.charAt(cnt)){
			case 'a':
				vocales[0]++;
				break;
			case 'e':
				vocales[1]++;
				break;
			case 'i':
				vocales[2]++;
				break;
			case 'o':
				vocales[3]++;
				break;
			case 'u':
				vocales[4]++;
				break;
			}
			cnt++;
		}while(cnt != fras.length());
		
		return vocales;
	}
	
	public static void wrFile(File fichero, int vocales[]) {
		try {
			FileWriter fw = new FileWriter(fichero);
			BufferedWriter bw = new BufferedWriter(fw);
			for(int cnt=0; cnt<=4; cnt++) {
				bw.write(vocales[cnt] + "\n");
			}
			bw.close();
		}catch(IOException e) {System.out.println(e.getMessage());}
		
		
		
	}

	public static void main(String[] args) {
		File fichero = new File("vocales.txt");
		String frase = "";
		int vocales[] = new int[4];

		frase = rdFrase();
		vocales = contVocales(frase);
		
		wrFile(fichero, vocales);
		
		
		
		
		

	}

}
