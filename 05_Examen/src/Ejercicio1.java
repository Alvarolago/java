import java.io.*;

public class Ejercicio1 {
	
	public static void leerFichero(File fichero, int[] tDni, String[] tNombres ) {
		/*Variables
		---------*/
		int letra,nEmpleados;
		String valorCadena;
		boolean numero, primero;
		
		try {
			FileReader fr = new FileReader(fichero);
			numero = primero = true;
			int cnt = 0;
			valorCadena = "";
			
			letra = fr.read();
			while(letra != -1) {
				if(letra != '-') {
					valorCadena += (char)letra;
				}else {
					if(numero == true) {
						if(primero == true) {
							nEmpleados = Integer.parseInt(valorCadena);
							primero = false;
						}else {
							tDni[cnt] = Integer.parseInt(valorCadena);
							cnt++;
						}
						numero = false;
						
					}else {
						tNombres[cnt] = valorCadena;
						numero = true;
					}
					valorCadena = "";
				}
				letra = fr.read();
			}
			fr.close();
			
		} catch (FileNotFoundException e) { e.printStackTrace(); 
		} catch (IOException e) { e.printStackTrace(); }
	}
	
	
	public static void escribirPantalla(int[] tDni, String[] tNombres) {
		System.out.println("El contenido es: ");
		for(int cnt = 0; cnt < tNombres.length; cnt++) {
			System.out.println(tNombres[cnt] + " - " + tDni);
		}
		
	}
	
	public static void mayorDni(String[] tNombres, int[] tDni) {
		int DNIMaximo = -100000;
		int posMax = 0;
		
		for(int cnt=0; cnt<tDni.length;cnt++)
			if(tDni[cnt] > DNIMaximo) {
				DNIMaximo = tDni[cnt];
				posMax = cnt;
			}
		System.out.println("El nombre del DNI mayor es " + tNombres[posMax]);
		
	}
	
	public static void menorDni(String[] tNombres, int[] tDni) {
		int DNIMinimo = 10000000;
		int posMin = 0;
		
		for(int cnt=0; cnt<tDni.length;cnt++)
			if((tDni[cnt] != 0) && (tDni[cnt] > DNIMinimo)) {
				DNIMinimo = tDni[cnt];
				posMin = cnt;
			}
		System.out.println("El nombre del DNI minimo es " + tNombres[posMin]);
		
	}
	
	
	

	public static void main(String[] args) {
		File fich = new File("Empleados.txt");

		int [] Dni = new int[10];
		String[] nombres = new String[10];
		
		leerFichero(fich, Dni, nombres);
		escribirPantalla(Dni, nombres);
		mayorDni(nombres, Dni);
		menorDni(nombres, Dni);

	}

}
