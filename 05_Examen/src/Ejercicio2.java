import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class Ejercicio2 {
	
	public static void crear(int numero1, int numero2) {
		int num = numero1;
		
		File carpeta = new File(Integer.toString(numero2));
		carpeta.mkdir();
		
		while(num <= numero2) {
			try {
				File fich = new File(carpeta, num + ".num");
				fich.createNewFile();
			} catch (IOException e) { e.printStackTrace(); }
			num++;
		}
	}
	
	

	public static void main(String[] args) {
		int numero1, numero2;
		Scanner num1 = new Scanner(System.in);
		
		System.out.println("Indique el 1� numero: ");
			numero1 = num1.nextInt();
		System.out.println("Indique el 2� numero: ");
			numero2 = num1.nextInt();
		
		crear(numero1, numero2);

	}

}
