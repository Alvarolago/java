import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;


public class Examen_Practica {

	public void createCarpet(String carName) {
		File carpet = new File(carName);
		carpet.mkdir();
	}
	
	public static void writeFileSimple(File fichero, String[] buffer) {
		
		try {
			FileWriter fw = new FileWriter(fichero);
			
			for(int cnt=0; cnt<buffer.length; cnt++)
				fw.write(buffer[cnt] + "\n");
			
			fw.close();
	
		
		}catch(IOException e) {e.getStackTrace(); e.getMessage();}
		
		
	}
	
	
	public static void writeFileBuffer(File fichero, String[] buffer) {
		
		try {
			FileWriter fw = new FileWriter(fichero);
			BufferedWriter bw = new BufferedWriter(fw);
			
			for(int cnt=0; cnt<buffer.length; cnt++) {
				bw.write(buffer[cnt]);
				bw.newLine();
			}
			bw.close();
		}catch(IOException e) {e.getStackTrace(); e.getMessage();}
		
		
	}
	
	public static String readFileSimple(File fichero) {
		
		String cadena="";
		
		try {
			FileReader fr = new FileReader(fichero);
			
			int letra=0;
			letra = fr.read();
			while(letra != -1) {
				cadena += (char) letra;
			}
			
			letra = fr.read();
			fr.close();
		}catch(IOException e) {e.getStackTrace();}
		
		System.out.println(cadena);
		return cadena;
		
	}
	
	public static String readFileBuffer(File fichero) {
		
		String cadena = "";
		
		try {
			FileReader fr = new FileReader(fichero);
			BufferedReader br = new BufferedReader(fr);
			String linea = "";	
			linea = br.readLine();
			while(linea != null) {
				cadena += linea;
			}
			br.close();
		}catch(IOException e) {e.getStackTrace();}
		
		return cadena;
	}
	
	
	
	
	public static void main(String[] args) {
		
		
		
		String[] cadTexto = new String[3];
		cadTexto[0] = "Me llamo alvaro";
		cadTexto[1] = "Y me gusta el futbol";
		cadTexto[2] = "Soy del Madrid";
		
		File fichero = new File("fichero.txt");
		String texto1 = "", texto2 = "";
		
		/*ESCRITURA EN LOS FICHEROS
		-------------------------*/
		//writeFileSimple(fichero, cadTexto);
		//writeFileBuffer(fichero, cadTexto);
		
		
		/*LECTURA DE LOS FICHEROS
		-----------------------*/
		//texto1 = readFileSimple(fichero);
		System.out.println("Fichero" + readFileSimple(fichero));
		
		//texto2 = readFileBuffer(fichero);
		

	}

}
