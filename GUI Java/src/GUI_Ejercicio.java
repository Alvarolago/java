import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JButton;
import java.awt.BorderLayout;
import javax.swing.SwingConstants;
import javax.swing.JPanel;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import javax.swing.BoxLayout;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.RowSpec;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JTextField;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JTextPane;
import java.awt.Label;
import javax.swing.JLabel;

public class GUI_Ejercicio {

	private JFrame frmCalculadora;
	private JTextField resultado;
	private char op= ' '; 
	private int posicion;
	private String num1,num2;
	private double total;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GUI_Ejercicio window = new GUI_Ejercicio();
					window.frmCalculadora.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public GUI_Ejercicio() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmCalculadora = new JFrame();
		frmCalculadora.setTitle("Calculadora");
		frmCalculadora.setBounds(100, 100, 540, 420);
		frmCalculadora.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JButton boton1 = new JButton("1");
		boton1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				resultado.setText(resultado.getText() + "1");
				
			}
		});
		
		JButton boton2 = new JButton("2");
		boton2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				resultado.setText(resultado.getText() + "2");
				
			}
		});
		
		JButton boton3 = new JButton("3");
		boton3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				resultado.setText(resultado.getText() + "3");
			}
		});
		
		JButton boton4 = new JButton("4");
		boton4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				resultado.setText(resultado.getText() + "4");
			}
		});
		
		JButton boton5 = new JButton("5");
		boton5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				resultado.setText(resultado.getText() + "5");
			}
		});
		
		JButton boton6 = new JButton("6");
		boton6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				resultado.setText(resultado.getText() + "6");
			}
		});
		
		JButton boton7 = new JButton("7");
		boton7.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				resultado.setText(resultado.getText() + "7");
			}
		});
		
		JButton boton8 = new JButton("8");
		boton8.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				resultado.setText(resultado.getText() + "8");
			}
		});
		
		JButton boton9 = new JButton("9");
		boton9.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				resultado.setText(resultado.getText() + "9");
			}
		});
		
		JButton botonmenos = new JButton("-");
		
		JButton botonmas = new JButton("+");
		
		JButton botonigual = new JButton("=");
		botonigual.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				switch(op) {
				case '+':
					posicion = resultado.getText().indexOf('+');
					num1 = resultado.getText().substring(0,posicion);
					num2 = resultado.getText().substring(posicion+1, resultado.getText().length());
					total = Double.parseDouble(num1) + Double.parseDouble(num2);
					resultado.setText(Double.toString(total));
					break;
				case '-':
					posicion = resultado.getText().indexOf('-');
					num1 = resultado.getText().substring(0,posicion);
					num2 = resultado.getText().substring(posicion+1, resultado.getText().length());
					total = Double.parseDouble(num1) - Double.parseDouble(num2);
					resultado.setText(Double.toString(total));
					break;
				case '*':
					posicion = resultado.getText().indexOf('*');
					num1 = resultado.getText().substring(0,posicion);
					num2 = resultado.getText().substring(posicion+1, resultado.getText().length());
					total = Double.parseDouble(num1) * Double.parseDouble(num2);
					resultado.setText(Double.toString(total));
					break;
				case '/':
					posicion = resultado.getText().indexOf('/');
					num1 = resultado.getText().substring(0,posicion);
					num2 = resultado.getText().substring(posicion+1, resultado.getText().length());
					total = Double.parseDouble(num1) / Double.parseDouble(num2);
					resultado.setText(Double.toString(total));
					break;					
				}

	
			}
		});
		
		JButton botonmulti = new JButton("*");
		
		JButton botondiv = new JButton("/");
		botondiv.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		
		JLabel lblCalculadora = new JLabel("CALCULADORA");
		
		resultado = new JTextField();
		resultado.setColumns(10);
		
		JButton botonpunto = new JButton(".");
		
		JButton btnNewButton = new JButton("C");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				resultado.setText("");
				botonmenos.setEnabled(true);
				botonmas.setEnabled(true);
				botonmulti.setEnabled(true);
				botonmas.setEnabled(true);
				botondiv.setEnabled(true);
			}
		});
		GroupLayout groupLayout = new GroupLayout(frmCalculadora.getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(123)
					.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
						.addGroup(groupLayout.createSequentialGroup()
							.addPreferredGap(ComponentPlacement.RELATED, 93, Short.MAX_VALUE)
							.addComponent(lblCalculadora, GroupLayout.PREFERRED_SIZE, 118, GroupLayout.PREFERRED_SIZE)
							.addGap(190))
						.addGroup(groupLayout.createSequentialGroup()
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addGroup(groupLayout.createSequentialGroup()
									.addComponent(resultado, GroupLayout.PREFERRED_SIZE, 223, GroupLayout.PREFERRED_SIZE)
									.addPreferredGap(ComponentPlacement.RELATED)
									.addComponent(btnNewButton, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
								.addGroup(groupLayout.createSequentialGroup()
									.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING, false)
										.addComponent(botonpunto, 0, 0, Short.MAX_VALUE)
										.addComponent(botondiv, 0, 0, Short.MAX_VALUE)
										.addComponent(botonmulti, GroupLayout.PREFERRED_SIZE, 53, GroupLayout.PREFERRED_SIZE))
									.addPreferredGap(ComponentPlacement.RELATED)
									.addGroup(groupLayout.createParallelGroup(Alignment.LEADING, false)
										.addComponent(botonigual, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
										.addGroup(groupLayout.createSequentialGroup()
											.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
												.addGroup(groupLayout.createSequentialGroup()
													.addComponent(boton7, GroupLayout.PREFERRED_SIZE, 51, GroupLayout.PREFERRED_SIZE)
													.addPreferredGap(ComponentPlacement.RELATED)
													.addComponent(boton8, GroupLayout.PREFERRED_SIZE, 51, GroupLayout.PREFERRED_SIZE)
													.addPreferredGap(ComponentPlacement.RELATED)
													.addComponent(boton9, GroupLayout.PREFERRED_SIZE, 51, GroupLayout.PREFERRED_SIZE))
												.addGroup(groupLayout.createSequentialGroup()
													.addComponent(boton4, GroupLayout.PREFERRED_SIZE, 51, GroupLayout.PREFERRED_SIZE)
													.addPreferredGap(ComponentPlacement.RELATED)
													.addComponent(boton5, GroupLayout.PREFERRED_SIZE, 51, GroupLayout.PREFERRED_SIZE)
													.addPreferredGap(ComponentPlacement.RELATED)
													.addComponent(boton6, GroupLayout.PREFERRED_SIZE, 51, GroupLayout.PREFERRED_SIZE))
												.addGroup(groupLayout.createSequentialGroup()
													.addComponent(boton1, GroupLayout.PREFERRED_SIZE, 51, GroupLayout.PREFERRED_SIZE)
													.addPreferredGap(ComponentPlacement.RELATED)
													.addComponent(boton2, GroupLayout.PREFERRED_SIZE, 51, GroupLayout.PREFERRED_SIZE)
													.addPreferredGap(ComponentPlacement.RELATED)
													.addComponent(boton3, GroupLayout.PREFERRED_SIZE, 51, GroupLayout.PREFERRED_SIZE)))
											.addPreferredGap(ComponentPlacement.RELATED)
											.addGroup(groupLayout.createParallelGroup(Alignment.LEADING, false)
												.addComponent(botonmas, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
												.addComponent(botonmenos, GroupLayout.PREFERRED_SIZE, 52, GroupLayout.PREFERRED_SIZE))))))
							.addContainerGap(119, Short.MAX_VALUE))))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(98)
					.addComponent(lblCalculadora, GroupLayout.PREFERRED_SIZE, 68, GroupLayout.PREFERRED_SIZE)
					.addGap(6)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(resultado, GroupLayout.PREFERRED_SIZE, 28, GroupLayout.PREFERRED_SIZE)
						.addComponent(btnNewButton, GroupLayout.PREFERRED_SIZE, 28, GroupLayout.PREFERRED_SIZE))
					.addGap(11)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(boton7)
						.addComponent(boton8)
						.addComponent(boton9)
						.addComponent(botonmenos, GroupLayout.PREFERRED_SIZE, 23, GroupLayout.PREFERRED_SIZE)
						.addComponent(botonmulti, GroupLayout.PREFERRED_SIZE, 23, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING, false)
						.addGroup(groupLayout.createSequentialGroup()
							.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
								.addComponent(boton4)
								.addComponent(boton5)
								.addComponent(boton6))
							.addPreferredGap(ComponentPlacement.RELATED)
							.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
								.addComponent(boton1)
								.addComponent(boton2)
								.addComponent(boton3)))
						.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
							.addComponent(botonmas, GroupLayout.PREFERRED_SIZE, 52, GroupLayout.PREFERRED_SIZE)
							.addComponent(botondiv, GroupLayout.PREFERRED_SIZE, 52, GroupLayout.PREFERRED_SIZE)))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING, false)
						.addComponent(botonpunto, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addComponent(botonigual, GroupLayout.PREFERRED_SIZE, 39, GroupLayout.PREFERRED_SIZE))
					.addContainerGap(44, Short.MAX_VALUE))
		);
		frmCalculadora.getContentPane().setLayout(groupLayout);
		

		botonmas.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				resultado.setText(resultado.getText()+"+");
				botonmenos.setEnabled(false);
				botonmas.setEnabled(false);
				botonmulti.setEnabled(false);
				botondiv.setEnabled(false);
				op = '+';
			}
		});
		botonmenos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				resultado.setText(resultado.getText()+"-");
				botonmas.setEnabled(false);
				botonmas.setEnabled(false);
				botonmulti.setEnabled(false);
				botondiv.setEnabled(false);
				op = '-';
			}
		});
		
		botonmulti.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				resultado.setText(resultado.getText()+"*");
				botonmenos.setEnabled(false);
				botonmas.setEnabled(false);
				botonmas.setEnabled(false);
				botondiv.setEnabled(false);
				op = '*';
			}
		});
		
		botondiv.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				resultado.setText(resultado.getText()+"/");
				botonmenos.setEnabled(false);
				botonmas.setEnabled(false);
				botonmulti.setEnabled(false);
				botonmas.setEnabled(false);
				op = '/';
			}
		});

	}

}
