import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JButton;
import java.awt.BorderLayout;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.ImageIcon;
import javax.swing.JCheckBox;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JTextArea;
import javax.swing.JPasswordField;
import java.awt.Toolkit;
import com.toedter.calendar.JCalendar;
import com.toedter.calendar.JDateChooser;
import javax.swing.JPanel;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class GUI {

	private JFrame frame;
	private JTextField txtIntroduzcaSuNombre;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private JPasswordField passwordField;
	private JTextField textField;
	private JPasswordField passwordField_1;

	/**
	 * Launch the application.
	 */
	//Un JFrame es un contenedor
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GUI window = new GUI();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public GUI() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setIconImage(Toolkit.getDefaultToolkit().getImage("C:\\Users\\CFGS\\Desktop\\2.png"));
		frame.setResizable(false);
		frame.setBounds(100, 100, 586, 540);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		frame.setLocationRelativeTo(null);
		
		JButton btnBotn = new JButton("Bot\u00F3n");
		btnBotn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btnBotn.setBounds(34, 48, 86, 20);
		frame.getContentPane().add(btnBotn);
		
		txtIntroduzcaSuNombre = new JTextField();
		txtIntroduzcaSuNombre.setToolTipText("Introduzca su nombre");
		txtIntroduzcaSuNombre.setBounds(140, 8, 152, 20);
		frame.getContentPane().add(txtIntroduzcaSuNombre);
		txtIntroduzcaSuNombre.setColumns(10);
		
		JLabel lblXd = new JLabel("Texto:");
		lblXd.setVerticalAlignment(SwingConstants.TOP);
		lblXd.setBounds(105, 11, 46, 14);
		frame.getContentPane().add(lblXd);
		
		JCheckBox chckbxEspaol = new JCheckBox("Espa\u00F1ol?\r\n");
		chckbxEspaol.setBounds(34, 98, 97, 23);
		frame.getContentPane().add(chckbxEspaol);
		
		JRadioButton rdbtnEwfij = new JRadioButton("Alumno");
		rdbtnEwfij.setBounds(293, 47, 109, 23);
		frame.getContentPane().add(rdbtnEwfij);
		
		JRadioButton rdbtnTgd = new JRadioButton("Profesor");
		rdbtnTgd.setBounds(293, 73, 109, 23);
		frame.getContentPane().add(rdbtnTgd);
		
		JRadioButton rdbtnHombre = new JRadioButton("Hombre");
		buttonGroup.add(rdbtnHombre);
		rdbtnHombre.setBounds(293, 197, 109, 23);
		frame.getContentPane().add(rdbtnHombre);
		
		JRadioButton rdbtnMujer = new JRadioButton("Mujer");
		buttonGroup.add(rdbtnMujer);
		rdbtnMujer.setBounds(293, 220, 109, 23);
		frame.getContentPane().add(rdbtnMujer);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"Real Madrid", "Valencia", "Extremadura", "Betis", "Sevilla"}));
		comboBox.setSelectedIndex(1);
		comboBox.setBounds(308, 114, 120, 20);
		frame.getContentPane().add(comboBox);
		
		JTextArea textArea = new JTextArea();
		textArea.setLineWrap(true);
		textArea.setBounds(160, 46, 97, 50);
		frame.getContentPane().add(textArea);
		
		passwordField = new JPasswordField();
		passwordField.setBounds(145, 98, 120, 19);
		frame.getContentPane().add(passwordField);
		
		JLabel lblNewLabel_1 = new JLabel("New label");
		lblNewLabel_1.setBounds(333, 257, 46, 14);
		frame.getContentPane().add(lblNewLabel_1);
		
		JCalendar calendar = new JCalendar();
		calendar.setBounds(108, 299, 184, 153);
		frame.getContentPane().add(calendar);
		
		JDateChooser dateChooser = new JDateChooser();
		dateChooser.setBounds(346, 332, 95, 20);
		frame.getContentPane().add(dateChooser);
		
		textField = new JTextField();
		
		textField.setBounds(444, 48, 86, 20);
		frame.getContentPane().add(textField);
		textField.setColumns(10);
		
		passwordField_1 = new JPasswordField();
		passwordField_1.setEnabled(false);
		passwordField_1.setBounds(444, 74, 86, 20);
		frame.getContentPane().add(passwordField_1);
		
		JComboBox comboBox_1 = new JComboBox();
		comboBox_1.setBounds(34, 141, 86, 20);
		frame.getContentPane().add(comboBox_1);
		
		JComboBox comboBox_2 = new JComboBox();
		comboBox_2.setBounds(374, 394, 28, 20);
		frame.getContentPane().add(comboBox_2);
		
		
		
		//Eventos
		textField.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				passwordField_1.setEnabled(true);
			}
		});
	}
}
