
public class Administracion extends Datos {
	private char sexo; //M --> man W--> woman
	private int HExtra;
	
	
	public Administracion(String dNI, String name, String surenames, double salary, char sexo, int hExtra) {
		super(dNI, name, surenames, salary);
		this.sexo = sexo;
		HExtra = hExtra;
	}
	
	
	public void mostrarDatos() {
		System.out.println("DNI:" + DNI + 
				"\nNombre:" + name +
				"\nApellidos:" + surenames +
				"\nSalario:" + salary + 
				"\nSexo:" + sexo +
				"\nHExtra:" + HExtra);
		System.out.println("\n");
	}
	
	

}
