
public class Datos {
	protected String DNI;
	protected String name;
	protected String surenames;
	protected double salary;
	
	
	//Method constructor
	public Datos(String dNI, String name, String surenames, double salary) {
		super();
		DNI = dNI;
		this.name = name;
		this.surenames = surenames;
		this.salary = salary;
	}


	
	//Getters and Setters

	public String getDNI() {
		return DNI;
	}



	public void setDNI(String dNI) {
		DNI = dNI;
	}



	public String getName() {
		return name;
	}



	public void setName(String name) {
		this.name = name;
	}



	public String getSurenames() {
		return surenames;
	}



	public void setSurenames(String surenames) {
		this.surenames = surenames;
	}



	public double getSalary() {
		return salary;
	}



	public void setSalary(double salary) {
		this.salary = salary;
	}
	
	
	
	
	
	
	
	
	

}
