
public class Principal {
	public static void main(String [] args) {
		//Introducimos los datos
		Profesor profesor = new Profesor("1256421S","Pepe","Garcia",1050,15,false);
		
		Administracion administracion = new Administracion("2453115P","Laura","Lopez",2000,'W',20);
		
		Directivo directivo = new Directivo("52445211D","Paco","yoquese",1500,false,'T');
		
		
		//Para imprimir datos por pantalla
		profesor.mostrarDatos();
		administracion.mostrarDatos();
		directivo.mostrarDatos();
	}
	

}
