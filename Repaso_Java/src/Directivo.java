
public class Directivo extends Datos{
	private boolean Salesiano;
	private char schedule; // Horario M--> Ma�ana T--> Tarde
	
	
	
	public Directivo(String dNI, String name, String surenames, double salary, boolean salesiano, char schedule) {
		super(dNI, name, surenames, salary);
		Salesiano = salesiano;
		this.schedule = schedule;
	}
	
	
	public void mostrarDatos() {
		System.out.println("DNI:" + DNI + 
				"\nNombre:" + name +
				"\nApellidos:" + surenames +
				"\nSalario:" + salary + 
				"\nSalesiano:" + Salesiano +
				"\nHorario:" + schedule);
		System.out.println("\n");
	}
	
	
	
	
	

}
