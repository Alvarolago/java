
public class Profesor extends Datos{
	private int NSubjects;
	private boolean tutor;
	
	
	
	//Constructor super class
	public Profesor(String dNI, String name, String surenames, double salary, int nSubjects, boolean tutor) {
		super(dNI, name, surenames, salary);
		NSubjects = nSubjects;
		this.tutor = tutor;
	}
	

	
	public void mostrarDatos() {
		System.out.println("DNI:" + DNI + 
							"\nNombre:" + name +
							"\nApellidos:" + surenames +
							"\nSalario:" + salary + 
							"\nNumero Asignaturas:" + NSubjects +
							"\nEs tutor:" + tutor);
		System.out.println("\n");
	}
	
	

}
