
public class Articulo {
	private String nombArticulo;
	private String precio;
	
	public Articulo() {
		nombArticulo = "";
		precio = "";
	}
	
	public Articulo (String nmArticulo, String prec) {
		nombArticulo = nmArticulo;
		precio = prec;
	}

	/*GETTER && SETTERS*/
	public String getNombArticulo() {
		return nombArticulo;
	}

	public void setNombArticulo(String nombArticulo) {
		this.nombArticulo = nombArticulo;
	}

	public String getPrecio() {
		return precio;
	}

	public void setPrecio(String precio) {
		this.precio = precio;
	}
	
	

}
