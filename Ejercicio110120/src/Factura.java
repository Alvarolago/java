
public class Factura {
	private String idFactura;
	private String nArticulos;
	
	public Factura(){
		idFactura = "";
		nArticulos = "";
	}
	
	public Factura(String id, String num) {
		idFactura = id;
		nArticulos = num;
	}

	
	/*GETTER && SETTERS*/
	public String getIdFactura() {
		return idFactura;
	}

	public void setIdFactura(String idFactura) {
		this.idFactura = idFactura;
	}

	public String getnArticulos() {
		return nArticulos;
	}

	public void setnArticulos(String nArticulos) {
		this.nArticulos = nArticulos;
	}
	
	
	

}
